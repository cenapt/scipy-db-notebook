# scipy-db-notebook


Based on https://hub.docker.com/r/jupyter/scipy-notebook it adds:

Modified 2018-02-12:
- Installing the latest beakerx
- Adding pyld
- Adding elasticsearch
- Grouping the conda installs

Modified 2018-01-30:
- Make JupyterLab the default command (you can still access notebooks by /notebooks)

Added 2018-01-29:
- BeakerX/JupyterLab support (with Scala, Groovy, and so on) 
- Ipython clusters

Before 2018:
- DB support for Postgres (sqlalchemy), Mongo (pymongo)
- Bio (BioPython and openbabel)
- Semantic web (rdflib, rdflib-jsonld)
- AWS/S3 support (boto3)
- Textmining (nltk)

## Basic use

docker run -it --rm -p 8888:8888 cenapt/scipy-db-notebook

